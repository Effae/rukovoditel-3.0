<?php

    //файл
    $name       = explode(".", $_FILES["file"]["name"]);
    $mime       = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $_FILES["file"]["tmp_name"]);
    $extension  = end($name);

    //путь сохранения
    $path       = "/uploads/wswg_attachments";
    $date       = new DateTime();

    $dirY       = "/" . $date->format("Y");
    $dirM       = "/" . $date->format("m");
    $dirD       = "/" . $date->format("d");

    //проверка существования пути
    if( !is_dir(getcwd() .$path) )
        mkdir(getcwd() .$path);
    if( !is_dir(getcwd() .$path . $dirY) )
        mkdir(getcwd() .$path . $dirY);
    if( !is_dir(getcwd() .$path . $dirY . $dirM) )
        mkdir(getcwd() .$path . $dirY . $dirM);
    if( !is_dir(getcwd() .$path . $dirY . $dirM . $dirD) )
        mkdir(getcwd() .$path . $dirY . $dirM . $dirD);

    $fullPath       = $path . $dirY . $dirM . $dirD;

    //сохранения файла
    $saveName       = $date->getTimestamp() . "_" . array_shift($name). "." . $extension;
    $result         = move_uploaded_file($_FILES["file"]["tmp_name"], getcwd() . $fullPath . "/" . $saveName);
    $status         = "success";
    if( !$result ) $status = "error";

    //ответ
    $response               = new StdClass;
    $response->status       = $status;
    $response->location     = $fullPath . "/" . $saveName;
    echo stripslashes(json_encode($response,JSON_UNESCAPED_UNICODE));

?>
